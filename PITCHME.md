---
# Stack - What is
- LIFO - last in first out
- Big O

| |Push|Pop|Search|Access|
|:---:|:---:|:---:|:---:|:---:|
|Big O|O(1)|O(1)|O(n)|O(n)|

---
# Stack - Setup
```java
public class BasicStack<E> {
    
    public void push(E value) {}

    public E pop() {}

    public boolean search(E value) {}

    public E access(E value) {}
}

```
---
# Stack - core java
- Stack<E> https://docs.oracle.com/javase/8/docs/api/java/util/Stack.html
- Deque<E> https://docs.oracle.com/javase/8/docs/api/java/util/Deque.html
---
# Challenge
https://www.hackerrank.com/challenges/maximum-element/problem

---
# Queue - What is
- FIFO - first in first out
- Big O

| |EnQueue|DeQueue|Search|Access|
|:---:|:---:|:---:|:---:|:---:|
|Big O|O(1)|O(1)|O(n)|O(n)|

---
# Queue - Setup
```java
public class BasicQueue<E> {
    
    public void queue(E value) {}

    public E enQueue() {}

    public boolean search(E value) {}

    public E access(E value) {}
}
```
---
# Queue - core java
- Queue<E> https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html

---
# Challenge
https://www.hackerrank.com/challenges/queue-using-two-stacks/problem

---
# List - What is
- Collection of node, dont have rule for getting data
- Big O

| |add|remove|insert|removeAt|search|get|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Big O|O(1)|O(1)|O(n)|O(n)|O(n)|O(n)|

---
# List - Setup
```java
public class BasicLinkList<E> {
    private class Node {
        private Node next;
        private X item;
        
        public Node(X item) {
            this.item = item;
        }
    }
    
    public void add(X item) { }
    
    public X remove() { }
    
    public void insert(X item, int position) { }
    
    public X removeAt(int position) { }
    
    public X get(int position) { }
    
    public int find(X item) { }
    
}
```

---
# List - core java
- List<E> https://docs.oracle.com/javase/8/docs/api/java/util/List.html

---
# Challenge
https://www.hackerrank.com/challenges/detect-whether-a-linked-list-contains-a-cycle/problem

---
# Hash - What is
- Is the map of key, value
- Dont have order

---
# Hash - determine the hash value
- hash is the map from object to int
- hashCode()

---
# Hash - set up
```java
public class BasicHashTable<K, V> {
    public int getSize() {}

    public V get(K key) {}

    public void put(K key, V value) {}

    public V delete(K key) {}

    public boolean hasKey(K key) { }

    public boolean hasValue(V value) {}
}
```

---
# Hash - java core
- Map<K, V> - https://docs.oracle.com/javase/8/docs/api/java/util/Map.html

---
# Challenge

---
# Tree - What is
- Set of node have relationship, each node can be parent, child or both
- Binary tree is the tree, each node have maximum 2 children is left, and right

---
# Tree - set up
```java
public class BasicBinaryTree<E> {
    public int getSize() {}
    
    public void add(E item) {}
    
    public void delete(E item) {}
    
    public boolean contains(E item) {}
}
```

---
# Tree - Java core
- TreeMap<K, V) - https://docs.oracle.com/javase/8/docs/api/java/util/TreeMap.html