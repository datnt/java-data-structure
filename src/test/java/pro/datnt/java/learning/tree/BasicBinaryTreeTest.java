package pro.datnt.java.learning.tree;

import org.junit.Test;

import static org.junit.Assert.*;

public class BasicBinaryTreeTest {

    @Test
    public void getSize() {
    }

    @Test
    public void add() {
        BasicBinaryTree<Integer> tree = new BasicBinaryTree<>();
        tree.add(10);
        tree.add(50);
        tree.add(40);
        assertEquals(3, tree.getSize());
        assertTrue(tree.contains(50));
        assertFalse(tree.contains(90));
        tree.delete((50));

        assertFalse(tree.contains(50));
        assertEquals(2, tree.getSize());
    }
}