package pro.datnt.java.learning.list;

import org.junit.Test;

import static org.junit.Assert.*;

public class BasicLinkListTest {

    @Test
    public void add() {
        BasicLinkedList<Integer> list = new BasicLinkedList<>();
        list.add(5);
        assertEquals(list.size(), 1);

        Integer removeItem = list.remove();
        assertEquals(0, list.size());
        assertEquals(5, removeItem.longValue());

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(6);
        list.insert(5, 4);
        assertEquals(5, Long.valueOf(list.get(5)).longValue());

    }

    @Test(expected = IllegalStateException.class)
    public void removeEmpty() {
        BasicLinkedList<Integer> list = new BasicLinkedList<>();
        list.remove();
    }

    @Test(expected = IllegalStateException.class)
    public void insertEmpty() {
        BasicLinkedList<Integer> list = new BasicLinkedList<>();
        list.insert(5, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getOutOfRange() {
        BasicLinkedList<Integer> list = new BasicLinkedList<>();
        list.add(5);
        list.get(6);
    }
}