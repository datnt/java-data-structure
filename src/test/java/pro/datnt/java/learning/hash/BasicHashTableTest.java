package pro.datnt.java.learning.hash;

import org.junit.Test;

import static org.junit.Assert.*;

public class BasicHashTableTest {

    @Test
    public void get() {
        BasicHashTable<String, String> hashTable = new BasicHashTable<>(50);

        hashTable.put("Monday", "Recover running");
        assertEquals("Recover running", hashTable.get("Monday"));
        assertEquals(1, hashTable.getSize());
        hashTable.delete("Monday");
        assertNull(hashTable.get("Monday"));
        hashTable.put("Tuesday", "Rest");
        hashTable.put("Wednesday", "Threshold running");
        assertTrue(hashTable.hasKey("Tuesday"));
        assertFalse(hashTable.hasKey("Friday"));
        assertTrue(hashTable.hasValue("Rest"));
    }
}