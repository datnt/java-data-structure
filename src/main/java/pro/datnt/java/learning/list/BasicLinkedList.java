package pro.datnt.java.learning.list;

public class BasicLinkedList<X> {
    private Node first;

    private Node last;

    private int nodeCount;

    private class Node {
        private Node next;
        private X item;

        public Node(X item) {
            this.item = item;
        }
    }

    public BasicLinkedList() {

    }

    public void add(X item) {
        Node newNode = new Node(item);
        if (first == null) {
            first = newNode;
            last = first;
        } else {
            last.next = newNode;
            last = newNode;
        }
        nodeCount++;
    }

    public int size() {
        return nodeCount;
    }

    public X remove() {
        if (first == null) {
            throw new IllegalStateException("The remove function is forbidden for the empty list");
        }

        Node removeNode = first;
        first = first.next;
        nodeCount--;

        return removeNode.item;
    }

    public void insert(X item, int position) {
        if (first == null) {
            throw new IllegalStateException("The insert function is forbidden for the empty list");
        }

        Node currentNode = first;

        for (int i = 1; i < position && currentNode.next != null; i++) {
            currentNode = currentNode.next;
        }
        Node newNode = new Node(item);
        newNode.next = currentNode.next;
        currentNode.next = newNode;
        nodeCount++;
    }

//    public X removeAt(int position) { }
//
    public X get(int position) {
        if (size() < position) {
            throw new IllegalArgumentException("The position is invalid");
        }

        Node currentNode = first;

        for (int i = 1; i < size() && currentNode != null; i++) {
            if (i == position) {
                return currentNode.item;
            }
            currentNode = currentNode.next;
        }

        return null;
    }

//    public int find(X item) { }

}
