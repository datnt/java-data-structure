package pro.datnt.java.learning.stack;

import java.util.Deque;
import java.util.Stack;

public class BasicStack<E> {
    private E[] data;
    private int index;

    public void push(E value) {
        data[index++] = value;
    }

    public E pop() {
        if (index == 0) {
            throw new IllegalStateException("Data is empty");
        }

        return data[--index];
    }

    public boolean search(E value) {
        for (int i = 0; i < index; i++) {
            if (data[i].equals(value)) {
                return true;
            }
        }

        return false;
    }

    public E access(E value) {
        while (index > 0) {
            E item = pop();
            if (item.equals(value)) {
                return item;
            }
        }

        throw new  IllegalStateException("Could not find the item " + value);
    }
}
