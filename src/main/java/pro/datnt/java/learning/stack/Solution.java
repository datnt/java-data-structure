package pro.datnt.java.learning.stack;
import java.io.*;
import java.util.*;

//https://www.hackerrank.com/challenges/maximum-element/problem
public class Solution {

    private static Stack<DataNode> sequence = new Stack<>();

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int max = Integer.MIN_VALUE;
        int totalNumber = sc.nextInt();
        for (int i = 0; i < totalNumber; i++) {
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    int value = sc.nextInt();
                    max = Math.max(value, max);
                    DataNode dn = new DataNode(value, max);
                    sequence.push(dn);
                    break;
                case 2:
                    sequence.pop();
                    if (sequence.empty()) {
                        max = Integer.MIN_VALUE;
                    } else {
                        max = sequence.peek().currentMax;
                    }
                    break;
                case 3:
                    if (!sequence.empty()) {
                        System.out.println(sequence.peek().currentMax);
                    }
                    break;
            }
        }
        sc.close();

    }

    private static class DataNode {
        private int value;
        private int currentMax;
        private DataNode(int value, int currentMax) {
            this.currentMax = currentMax;
            this.value = value;
        }
    }
}