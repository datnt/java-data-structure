package pro.datnt.java.learning.queue;

import java.util.Queue;

public class BasicQueue<X> {
    private X[] data;
    private int front;
    private int end;

    public BasicQueue() {
        this(1000);
    }

    public BasicQueue(int size) {
        data = (X[])new Object[size];
        front = -1;
        end = -1;
    }

    public void enQueue(X value) {
        if ((end + 1) % data.length == front) {
            throw new IllegalStateException("The queue is full");
        }

        if (size() == 0) {
            front++;
            end++;
            data[end] = value;
        } else {
            end++;
            data[end] = value;
        }
    }

    public X deQueue() {
        X item = null;
        if (size() == 0) {
            throw new IllegalStateException("The queue is empty");
        } else if (front == end) {
            item = data[front];
            data[front] = null;
            front = -1;
            end = -1;
        } else {
            item = data[front];
            data[front] = null;
            front++;
        }

        return item;
    }

    public int size() {
        if (front == -1 && end == -1) {
            return 0;
        }

        return end - front + 1;
    }
}
