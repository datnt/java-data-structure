package pro.datnt.java.learning.queue;

import java.util.*;

//https://www.hackerrank.com/challenges/queue-using-two-stacks/problem
public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        Queue<Integer> data = new ArrayDeque<>();
        int total = sc.nextInt();
        for (int i = 0; i < total; i++) {
            byte command = sc.nextByte();
            switch (command) {
                case 1:
                    int value = sc.nextInt();
                    data.add(value);
                    break;
                case 2:
                    data.remove();
                    break;
                case 3:
                    System.out.println(data.peek());
                    break;
            }
        }
    }
}