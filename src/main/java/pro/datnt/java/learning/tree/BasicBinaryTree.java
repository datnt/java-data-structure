package pro.datnt.java.learning.tree;

public class BasicBinaryTree<E extends Comparable<E>> {
    private class Node {
        private E item;
        private Node left;
        private Node right;
        private Node parent;

        public Node(E item) {
            this.item = item;
        }
    }

    private Node root;
    private int size;

    public int getSize() {
        return size;
    }

    public void add(E item) {
        Node node = new Node(item);
        if (root == null) {
            root = node;
            size++;
            return;
        }
        insert(root, node);
    }

    public boolean contains(E item) {
        Node node = getNode(item);
        return node != null;
    }

    private Node getNode(E item) {
        Node node = root;

        while (node != null) {
            int val = item.compareTo(node.item);
            if (val == 0) {
                return node;
            }
            if (val < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }

        return null;
    }

    private void unlink(Node currentNode, Node newNode) {
        if (this.root == currentNode) {
            if (newNode != null) {
                newNode.left = currentNode.left;
                newNode.right = currentNode.right;
            }
            this.root = newNode;
        } else if (currentNode.parent.right == currentNode) {
            currentNode.parent.right = newNode;
        } else if (currentNode.parent.left == currentNode) {
            currentNode.parent.left = newNode;
        }
    }

    public boolean delete(E item) {
        if (this.root == null) {
            return false;
        }

        Node currentNode = getNode(item);
        if (currentNode == null) {
            return false;
        }

        if (currentNode.left != null && currentNode.right == null) {
            unlink(currentNode, currentNode.left);
        } else if (currentNode.left == null && currentNode.right != null) {
            unlink(currentNode, currentNode.right);
        } else if (currentNode.left != null) {
            Node left = currentNode.left;
            while (left.right != null) {
                left = left.right;
            }

            left.parent.right = null;
            left.left = currentNode.left;
            left.right = currentNode.right;
            unlink(currentNode, left);
        } else {
            unlink(currentNode, null);
        }

        size--;
        return true;
    }

    private void insert(Node parent, Node child) {
        // less than parent, add to left node
        if (child.item.compareTo(parent.item) < 0) {
            if (parent.left == null) {
                child.parent = parent;
                parent.left = child;
                size++;
                return;
            }
            insert(parent.left, child);
            return;
        }

        //greater than parent, add to right node
        if (child.item.compareTo(parent.item) > 0) {
            if (parent.right == null) {
                child.parent = parent;
                parent.right = child;
                size++;
                return;
            }
            insert(parent.right, child);
        }
    }
}
