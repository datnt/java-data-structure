package pro.datnt.java.learning.hash;

public class BasicHashTable<K, V> {
    private HashEntry[] data;
    private int capacity;
    private int size;

    public BasicHashTable(int capacity) {
        this.capacity = capacity;
        data = new HashEntry[capacity];
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public V get(K key) {
        int hash = calculateHash(key);
        if (data[hash] == null) {
            return null;
        }

        return (V)data[hash].value;
    }

    public void put(K key, V value) {
        int hash = calculateHash(key);
        data[hash] = new HashEntry<>(key, value);
        size++;
    }

    public V delete(K key) {
        V value = get(key);
        if (value == null) {
            return null;
        }
        int hash = calculateHash(key);
        data[hash] = null;
        size--;

        hash = (hash + 1) % capacity;
        while (data[hash] != null) {
            HashEntry<K, V> hashEntry = data[hash];
            data[hash] = null;
            size--;
            put(hashEntry.key, hashEntry.value);
            hash = (hash + 1) % capacity;
        }


        return value;
    }

    public boolean hasKey(K key) {
        int hash = calculateHash(key);
        if (data[hash] == null) {
            return  false;
        }

        return key.equals(data[hash].key);
    }

    public boolean hasValue(V value) {
        for (int i = 0; i < capacity; i++) {
            if (data[i] != null && data[i].value.equals(value)) {
                return true;
            }
        }

        return false;
    }

    private int calculateHash(K key) {
        int hash = key.hashCode() % capacity;
        while (data[hash] != null && !key.equals(data[hash].key)) {
            hash = (hash + 1) % capacity;
        }

        return hash;
    }

    private class HashEntry<K, V> {
        private K key;
        private V value;

        public HashEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
